package com.stylefeng.guns.rest.modular.live.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class RedisSortUtil {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 获得key数组里面key2元素的索引
     * @param key
     * @param key2
     *
     * @return
     */
    public Long rank(String key, Object key2) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        return zset.rank(key, key2);
    }



    /**
     * 有序集合添加
     *
     * @param key
     * @param value
     * @param scoure
     */
    public void zAdd(String key, Object value, double scoure) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        zset.add(key, value, scoure);
    }



    /**
     * 获得key数组里面key2元素的排序值
     * @param key
     * @param key2
     * @return
     */
    public double score(String key, Object key2) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        return zset.score(key, key2);
    }



    /**
     * 从高到低的排序集中获取从头(start)到尾(end)内的元素。
     * @param key
     * @param start
     * @param end
     * @return
     */
    public Set<Object> reverseRange(String key, long start, long end) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        Set<Object> objects = zset.reverseRange(key, start, end);
        return objects;
    }
}

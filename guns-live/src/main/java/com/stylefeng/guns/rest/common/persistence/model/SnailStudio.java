package com.stylefeng.guns.rest.common.persistence.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 直播间表
 * </p>
 *
 * @author qifanlee
 * @since 2019-04-01
 */
@TableName("snail_studio")
@Data
public class SnailStudio extends Model<SnailStudio> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 房间号
     */
    private Integer guid;
    /**
     * 直播状态  1：直播中  2：直播已结束
     */
    private Integer status;
    /**
     * 直播内容简介
     */
    private String content;

    /**
     * 直播类型
     */
    private int type;

    /**
     * 观看人数
     */
    private int onlineNum;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SnailStudio{" +
        "id=" + id +
        ", uid=" + uid +
        ", guid=" + guid +
        ", status=" + status +
        "}";
    }
}

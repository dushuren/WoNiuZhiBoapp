package com.stylefeng.guns.rest.modular.live.vo;

import com.stylefeng.guns.api.live.vo.StudioVO;
import lombok.Data;

import java.util.List;

@Data
public class LiveIndexVO {

    private List<StudioVO> liveIndexInfo;
}

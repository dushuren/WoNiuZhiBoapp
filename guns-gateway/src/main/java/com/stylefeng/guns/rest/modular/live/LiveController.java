package com.stylefeng.guns.rest.modular.live;

import com.alibaba.dubbo.config.annotation.Reference;
import com.stylefeng.guns.api.live.LiveServiceAPI;
import com.stylefeng.guns.api.live.RedisSortAPI;
import com.stylefeng.guns.api.live.vo.IndexVO;
import com.stylefeng.guns.api.live.vo.SearchResultsVO;
import com.stylefeng.guns.api.live.vo.StudioVO;
import com.stylefeng.guns.api.user.vo.UserInfoModel;
import com.stylefeng.guns.rest.common.CurrentUser;
import com.stylefeng.guns.rest.modular.vo.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/live/")
@Slf4j
public class LiveController {

    @Reference(interfaceClass = LiveServiceAPI.class,check = false)
    private LiveServiceAPI liveServiceAPI;

    @Reference(interfaceClass = RedisSortAPI.class,check = false)
    private RedisSortAPI redisSortAPI;

    /**
     *远程操作流截图
     * @return
     */
    @GetMapping("getStreamPic")
    public String getStreamPic(){
        liveServiceAPI.ffmpegPhoto();
        return null;
    }

    /**
     * 获取首页信息
     * @return
     */
    @GetMapping("getIndexInfo")
    public ResponseVO getIndexInfo(){
        IndexVO indexInfo = liveServiceAPI.getIndexInfo();
        return ResponseVO.success(indexInfo);
    }

    @GetMapping(value = "getLiveByType")
    public ResponseVO getLiveByType(int type){
        List<StudioVO> liveByType = liveServiceAPI.getLiveByType(type);
        return ResponseVO.success(liveByType);
    }

    /**
     * 开启直播间
     * @return
     */
    @GetMapping("createLive")
    @Transactional
    public ResponseVO createLive(@RequestParam(value = "content",required = false) String content,
                                 @RequestParam(value = "type",required = true) int type){
        // 获取当前登陆用户
        String userId = CurrentUser.getCurrentUser();
        if(userId == null || userId.trim().length()< 1){
            return ResponseVO.serviceFail("请先登录！");
        }
        UserInfoModel user = new UserInfoModel();
        Integer guid = liveServiceAPI.checkLiveByUid(Integer.parseInt(userId));
        System.out.println(guid);
        if( guid == null || guid < 1){
            //不存在就创建
            int res = liveServiceAPI.careteLive(Integer.parseInt(userId),content,type);
            //更改用户身份为主播
            user.setUuid(Integer.parseInt(userId));
            user.setAnchor(2);
            int result = liveServiceAPI.updateAnchor(user);
            if(res <=0 || result <= 0){
                return ResponseVO.serviceFail("开启直播失败");
            }
            return ResponseVO.success(res);

        }else{
            //已存在就开启,更改状态为直播中
            int i = liveServiceAPI.updateStatus(Integer.parseInt(userId),type);
            int res = liveServiceAPI.updateContent(Integer.parseInt(userId), content);
            System.out.println(i+"---------------"+res);
            if(i>0 && res > 0){
                return ResponseVO.success(guid);
            }else{
                return ResponseVO.serviceFail("状态更改失败，请稍后再试");
            }

        }
    }

    /**
     * 关闭直播
     */
    @GetMapping("closeLive")
    public ResponseVO closeLive(){
        // 获取当前登陆用户
        String userId = CurrentUser.getCurrentUser();
        if(userId != null && userId.trim().length()>0){
            int res = liveServiceAPI.closeLive(Integer.parseInt(userId));
            if(res > 0){
                return ResponseVO.success("已关闭直播");
            }else{
                return ResponseVO.serviceFail("请稍后重试！");
            }
        }else{
            return ResponseVO.serviceFail("请稍后重试！");
        }
    }

    /**
     * 搜索
     */
    @PostMapping(value = "getSearchResult")
    public ResponseVO getSearchResult(String keyword){
        try {
            redisSortAPI.sort(keyword);
        }catch (Exception e){
            log.error("redis热词缓存出错："+e);
        }
        SearchResultsVO searchResultsVO = liveServiceAPI.searchByKeyword(keyword);
        return ResponseVO.success(searchResultsVO);
    }

    /**
     * redis热词推荐
     */
    @GetMapping(value = "getHot")
    public ResponseVO getHot(){
        Object zset = redisSortAPI.findZset(0, 9);
        return ResponseVO.success(zset);
    }
}

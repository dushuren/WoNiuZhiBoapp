package com.stylefeng.guns.rest.modular.websocket;

import com.alibaba.dubbo.config.annotation.Service;
import com.stylefeng.guns.api.websocket.WebSocketAPI;
import com.stylefeng.guns.rest.common.persistence.dao.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Service(interfaceClass = WebSocketAPI.class)
public class WebSocketServiceImpl implements WebSocketAPI {
    @Autowired
    private UserMapper userMapper;

    @Override
    public int updateOnlineNum(int onlineNum, String guid) {
        return userMapper.updateOnlineNum(onlineNum,guid);
    }
}

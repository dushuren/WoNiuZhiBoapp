package com.stylefeng.guns.rest.modular.websocket;

import com.alibaba.dubbo.config.annotation.Service;
import com.stylefeng.guns.rest.modular.websocket.vo.ChatServiceAPI;
import com.stylefeng.guns.api.websocket.vo.MessageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;


@Component
@Service(interfaceClass = ChatServiceAPI.class,timeout = 6000)
public class ChatServiceImpl implements ChatServiceAPI {
    public ChatServiceImpl(){
        System.out.println("------------ChatServiceImpl----------");
    }

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void test() {
        System.out.println(mongoTemplate);
    }

    @Override
    public void saveDemo(MessageVO demoEntity) {
        mongoTemplate.save(demoEntity);
    }

    @Override
    public void removeDemo(Long id) {
        mongoTemplate.remove(id);
    }

    @Override
    public void updateDemo(MessageVO demoEntity) {
        Query query = new Query(Criteria.where("id").is(demoEntity.getId()));
        Update update = new Update();
        update.set("fromUser", demoEntity.getFromUser());
        update.set("msg", demoEntity.getMsg());
        update.set("time", demoEntity.getTime());
        mongoTemplate.updateFirst(query, update, MessageVO.class);
    }

    @Override
    public MessageVO findDemoById(Long id) {
        Query query = new Query(Criteria.where("id").is(id));
        MessageVO demoEntity = mongoTemplate.findOne(query, MessageVO.class);
        return demoEntity;
    }
}

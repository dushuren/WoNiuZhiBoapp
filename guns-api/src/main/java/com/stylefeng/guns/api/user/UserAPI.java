package com.stylefeng.guns.api.user;

import com.stylefeng.guns.api.user.vo.*;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface UserAPI {

    int login(String username, String password);

    boolean register(UserModel userModel);

    boolean checkUsername(String username);

    boolean checkNickname(String nickname);

    UserInfoModel getUserInfo(int uuid);

    UserInfoModel updateUserInfo(UserInfoModel userInfoModel);

    int authentication(Integer uuid,UserValidRequestVO userValidRequestVO);

    //判断用户是否完成过实名认证
    boolean isNotValid(int uid);

    //判断身份证是否已被认证过
    boolean validIdCard(String idCard);

    //获取浏览记录
    Map getHistorys(int userId);

    //添加浏览记录
    int addHistory(String content,String picPath,String uid,String livePath,Integer cid,String guid);

    //点击关注
    int toFocus(int fromUserId,int toUserId);
    //取消
    int delFoucs(int fromUserId,int toUserId);

    //查看个人信息
    UserDetailInfo getUserDetail(int uid) throws ParseException;

    //获取任务列表
    JobVO getJobs(int uid);

    //点击报名
    int toJoin(int uid,int jobId);

    //获取任务id列表
    List<Integer> getJobIds();

    //统计任务数
    Integer countJobs();

    Integer countJobDetail(int uid);


}


package com.stylefeng.guns.api.user.vo;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
public class UserInfoModel implements Serializable{
    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;
    private Integer uuid;
    private String username;
    private String nickname;
    private long beginTime;
    private long updateTime;
    private Integer anchor;
    private Integer certStatus;
    private String headAddr;
    private Integer integrals;
    private Integer integralsZhu;


    public String getHeadAddr() {
        return headAddr;
    }

    public void setHeadAddr(String headAddr) {
        this.headAddr = headAddr;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(long beginTime) {
        this.beginTime = beginTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getAnchor() {
        return anchor;
    }

    public void setAnchor(Integer anchor) {
        this.anchor = anchor;
    }

    public Integer getCertStatus() {
        return certStatus;
    }

    public void setCertStatus(Integer certStatus) {
        this.certStatus = certStatus;
    }
}

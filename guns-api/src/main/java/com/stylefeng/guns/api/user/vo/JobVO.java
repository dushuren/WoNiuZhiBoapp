package com.stylefeng.guns.api.user.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class JobVO implements Serializable {
    /**
     * 用户任务
     */
    private List<JobList> userJobList;
    /**
     * 主播任务
     */
    private List<JobList> zhuJobList;
    /**
     * 活动任务
     */
    private List<JobList> actJobList;
}

package com.stylefeng.guns.api.websocket.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseVO implements Serializable {
    private String responseMessage;
    private String sender;
    private String type;
    private String content;

    public ResponseVO(String responseMessage) {
    }
    public ResponseVO(String sender, String type, String content) {
        this.sender = sender;
        this.type = type;
        this.content = content;
    }
}

package com.stylefeng.guns.api.live.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class RelatedUserVO implements Serializable {

    private String uuid;
    //头像地址
    private String headAddr;
    //昵称
    private String nickname;
    //关注度
    private int attention;
}

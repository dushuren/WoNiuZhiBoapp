package com.stylefeng.guns.api.user.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class HistoryInfoVO implements Serializable {

    private String time;
}

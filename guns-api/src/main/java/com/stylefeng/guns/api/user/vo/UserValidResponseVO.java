package com.stylefeng.guns.api.user.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserValidResponseVO implements Serializable {
    /**
     * 关联  user表的主键
     */
    private Integer uid;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 身份证号
     */
    private String idcard;
    /**
     * 性别
     */
    private String sex;
    /**
     * 身份证所在地
     */
    private String area;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区县
     */
    private String prefecture;
    /**
     * 出生年月
     */
    private String birthday;
    /**
     * 地区代码
     */
    private String addrCode;
    /**
     * 身份证校验码
     */
    private String lastCode;
}

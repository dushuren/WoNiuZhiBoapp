# 蜗牛直播app

#### Description
这是和伙伴合作完成的直播app，一个安卓，一个后台。后台采用基于springboot的guns开源框架搭建，设计为直播服务，user服务，websocket服务以及API网关服务。数据库使用了mongodb,redis和mysql。 比较有看头的是采用websocket+mongodb搭建了多人直播聊天室，用redis搭建了热搜排行榜等。在直播服务器上，选择了SRS，搭建简单，功能强大。开了两台学生机阿里云服务器，一台机的话顶不住srs+几个微服务。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
package com.stylefeng.guns.rest.common.persistence.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author qifanlee
 * @since 2019-06-10
 */
@TableName("snail_focus")
public class SnailFocus extends Model<SnailFocus> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 点击关注的那个用户
     */
    @TableField("from_user_id")
    private Integer fromUserId;
    /**
     * 被关注的用户
     */
    @TableField("to_user_id")
    private Integer toUserId;
    /**
     * 是否互粉
     */
    @TableField("both_status")
    private String bothStatus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Integer fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Integer getToUserId() {
        return toUserId;
    }

    public void setToUserId(Integer toUserId) {
        this.toUserId = toUserId;
    }

    public String getBothStatus() {
        return bothStatus;
    }

    public void setBothStatus(String bothStatus) {
        this.bothStatus = bothStatus;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SnailFocus{" +
        "id=" + id +
        ", fromUserId=" + fromUserId +
        ", toUserId=" + toUserId +
        ", bothStatus=" + bothStatus +
        "}";
    }
}

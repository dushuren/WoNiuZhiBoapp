package com.stylefeng.guns.rest.common.persistence.dao;

import com.stylefeng.guns.rest.common.persistence.model.SnailJobDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.common.util.MyMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qifanlee
 * @since 2019-06-12
 */
public interface SnailJobDetailMapper extends BaseMapper<SnailJobDetail> {

}
